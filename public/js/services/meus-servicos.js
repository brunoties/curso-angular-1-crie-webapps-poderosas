angular.module('meusServicos', ['ngResource'])
.factory('recursoFoto', function($resource) {
	return $resource('/v1/fotos/:fotoId', null, {
		'update' : {
			method: 'PUT'
		}
	});
})
.factory('cadastroFotoService', function($q, $rootScope, recursoFoto) {

	var service = {};

	var eventoFotoSalva = 'fotoSalva';

	service.cadastra = function(foto) {

		return $q(function(resolve, reject) {

			if(foto._id) {
				recursoFoto.update({fotoId: foto._id}, foto, function() {
					$rootScope.$broadcast(eventoFotoSalva);
					resolve({
						mensagem: 'Foto ' + foto.titulo + ' foi salva com sucesso',
						inclusao: false
					});
				}, function(erro) {
					console.log(erro);
					reject({
						mensagem: 'Falha ao tentar salvar a Foto'
					});
				});
			} else {
				recursoFoto.save(foto, function() {
					$rootScope.$broadcast(eventoFotoSalva);
					resolve({
						mensagem: 'Foto ' + foto.titulo + ' foi salva com sucesso',
						inclusao: true
					});
				}, function(erro) {
					console.log(erro);
					reject({
						mensagem: 'Falha ao tentar salvar a Foto'
					});
				});
			}

		});

	};

	return service;

});