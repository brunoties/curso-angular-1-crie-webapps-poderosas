//angular.module('alurapic').controller("FotosController", function($scope, $http) {
//angular.module('alurapic').controller("FotosController", function($scope, $resource) {
angular.module('alurapic').controller("FotosController", function($scope, recursoFoto) {

	//var recursoFoto = $resource('/v1/fotos/:fotoId');
	
	$scope.fotos = [];
	$scope.filtro = '';
	$scope.mensagem = '';

	
	recursoFoto.query(function(fotos) {
		$scope.fotos = fotos;
	}, function(erro) {
		console.log(erro);
	});

	/*
	$http.get('/v1/fotos')
		.success(function(fotos) {
			$scope.fotos = fotos;
		})
		.error(function(erro) {
			console.log(erro);
		});
	*/

	$scope.remover = function(foto) {
		recursoFoto.delete({fotoId: foto._id}, function() {
			var indiceFoto = $scope.fotos.indexOf(foto);
			$scope.fotos.splice(indiceFoto, 1);
			$scope.mensagem = 'Foto ' + foto.titulo + ' removida com sucesso';
		}, function(erro) {
			$scope.mensagem = 'Foto ' + foto.titulo + ' removida com sucesso';
			console.log('Não foi possível apagar a foto ' + foto.titulo);
		});
	}

	/*
	$scope.remover = function(foto) {

		$http.delete('v1/fotos/' + foto._id)
		.success(function() {
			var indiceFoto = $scope.fotos.indexOf(foto);
			$scope.fotos.splice(indiceFoto, 1);
			$scope.mensagem = 'Foto ' + foto.titulo + ' removida com sucesso';
			//console.log('Foto ' + foto.titulo + ' removida com sucesso');
		})
		.error(function(erro) {
			$scope.mensagem = 'Foto ' + foto.titulo + ' removida com sucesso';
			//console.log('Não foi possível apagar a foto ' + foto.titulo);
		});

	}; */
});

	/*
	$http.get('/v1/fotos').then(function(retorno) {
		$scope.fotos = retorno.data;
	}).catch(function(erro) {
		console.log(erro);
	});
	*/

	/*
	$scope.fotos = [
		{
			titulo : 'Leão 1',
			url : 'http://www.fundosanimais.com/Minis/leoes.jpg',
		},
		{
			titulo : 'Leão 2',
			url : 'http://www.fundosanimais.com/Minis/leoes.jpg',
		},
		{
			titulo : 'Leão 3',
			url : 'http://www.fundosanimais.com/Minis/leoes.jpg',
		},

	];
	*/

