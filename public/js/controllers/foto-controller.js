//angular.module('alurapic').controller('FotoController', function($scope, $http, $routeParams) {
//angular.module('alurapic').controller('FotoController', function($scope, $resource, $routeParams) {
/*
angular.module('alurapic').controller('FotoController', 
		function($scope, $routeParams, recursoFoto, cadastroFotoService) {
*/
//blindando-se contra minificação
/*
*Veja que o segundo parâmetro do controller é um array que recebe primeiro todos os artefatos 
*que o controller do Angular receberá e por último a função que define o controller. 
*O processo de minificação jamais tocará nos dados do array e o Angular segue a convenção que o 
*primeiro parâmetro do array será injetado como primeiro parâmetro da função do controller. 
*/
angular.module('alurapic').controller('FotoController', 
		['$scope', '$routeParams', 'recursoFoto', 'cadastroFotoService',
		function($scope, $routeParams, recursoFoto, cadastroFotoService) {


	//param 1: url + curinga
	//param 2: quando deseja usar query string, metodo get ex: ?parametro=valor
	// param 3: um objeto com todos atributos e metodos que desejar
	/*
	var recursoFoto = $resource('/v1/fotos/:fotoId', null, {
		'update' : {
			method: 'PUT'
		}
	});
	*/

	$scope.foto = {};
	$scope.mensagem = '';

	if($routeParams.fotoId) {
		recursoFoto.get({fotoId: $routeParams.fotoId}, function(foto) {
			$scope.foto = foto;
		}, function(erro) {
			console.log(erro);
			$scope.mensagem = 'Falha ao tentar editar a Foto';
		})
	}

	/*
	if($routeParams.fotoId) {
		$http.get('/v1/fotos/' + $routeParams.fotoId)
		.success(function(foto) {
			$scope.foto = foto;
		})
		.error(function(erro) {
			console.log(erro);
			$scope.mensagem = 'Falha ao tentar editar a Foto';
		});
	}
	*/

	$scope.submeter = function() {
		if($scope.form.$valid) {
			cadastroFotoService.cadastra($scope.foto)
			.then(function(dados) {
				$scope.mensagem = dados.mensagem;
				if(dados.inclusao) {
					$scope.foto = {};
				}
				//$scope.focado = true;
				//$scope.$broadcast('fotoSalva');
			})
			.catch(function(erro) {
				$scope.mensagem = erro.mensagem;
			});
		}
	};

	/*
	$scope.submeter = function() {

		if($scope.form.$valid) {

			if($routeParams.fotoId) {

				recursoFoto.update({fotoId: $scope.foto._id}, $scope.foto, function() {
					$scope.mensagem = 'Foto ' + $scope.foto.titulo + ' foi salva com sucesso';
				}, function(erro) {
					console.log(erro);
					$scope.mensagem = 'Falha ao tentar salvar a Foto';
				})

			} else {
				recursoFoto.save($scope.foto, function() {
					$scope.mensagem = 'Foto ' + $scope.foto.titulo + ' foi salva com sucesso';
					$scope.foto = {};
				}, function(erro) {
					console.log(erro);
					$scope.mensagem = 'Falha ao tentar salvar a Foto';
				})
			}

		}
	}
	*/

	/*
	$scope.submeter = function() {

		if($scope.form.$valid) {

			if($routeParams.fotoId) {

				$http.put('/v1/fotos/' + $scope.foto._id, $scope.foto)
				.success(function() {
					$scope.mensagem = 'Foto alterada com sucesso';
				})
				.error(function(erro) {
					console.log(erro);
					$scope.mensagem = 'Falha ao tentar alterar a Foto';
				});

			} else {

				$http.post('/v1/fotos', $scope.foto)
				.success(function() {
					$scope.mensagem = 'Foto salva com sucesso';
					$scope.foto = {};
				})
				.error(function() {
					$scope.mensagem = 'Falha ao tentar salvar a foto!';
				});

			}

		}

	};
	*/

}]);