angular.module('minhasDiretivas', ['meusServicos'])

	.directive('meuPainel', function() {

		var ddo = {};
		//diretiva pode ser usada tanto como atributo ou como elemento
		//exemplo elemento: <meu-painel></meu-painel>
		//exemplo atributo: <div meu-painel></div>
		/*
		*nome da nossa diretiva está em camelCase, porém na marcação HTML estamos usando hífen. 
		*Este é um padrão do Angular que não podemos deixar de seguir
		*/
		ddo.restrict = "AE";

		//Para que o Angular preserve o conteúdo original da diretiva, 
		//precisamos usar o mecanismo de transclusão.
		ddo.transclude = true;

		/*quando o nome do atributo na diretiva na marcação é igual ao nome da propriedade que 
		*guardará o seu valor, podemos deixar apenas @
		*/
		ddo.scope = {
			titulo: '@'
		};

		ddo.templateUrl = 'js/directives/meu-painel.html';

		return ddo;

	})
	.directive('minhaFoto', function() {

		var ddo = {};

		ddo.restrict = 'AE';

		ddo.scope = {
			titulo: '@',
			url: '@'
		};

		ddo.template = '<img class="img-responsive center-block" ng-src="{{url}}" '
			+ 'alt="{{titulo}}" title="{{titulo}}">';

		return ddo;
	})
	.directive('botaoRemover', function() {
		var ddo = {};

		ddo.restrict = "E";
		ddo.scope = {
			nome: '@',
			acao: '&'
		};

		ddo.template = '<button ng-click="acao()" class="btn btn-danger btn-block">{{nome}}</button>';

		return ddo;
	})
	/*
	*buscar sempre outra alternativa no lugar do watch, devido ao seu custo computacional
	*/
	.directive('setFocus', function() {
		var ddo = {};
		ddo.restrict = "A";
		/*
		ddo.scope = {
			focado: '=',
		};
		*/
		ddo.link = function(scope, element) {
			
			//para ouvir o evento fotoSalva
			scope.$on('fotoSalva', function() {
				element[0].focus();
			});
			/*
			// executado toda vez que o valor mudar
			//O $watch é mais inteligente ainda, podemos receber o valor atual e o valor antes da mudança
			// como parâmetros:
			/*
			 scope.$watch('focado', function(novoValor, valorAntigo) {
                  alert('mudei');
           });
           */

           /*

			scope.$watch('focado', function() {
				if(scope.focado) {
					element[0].focus();
					scope.focado = false;
				}
			}) 
			*/
		};
		return ddo;
	})
	.directive('meusTitulos', function() {
		var ddo = {};
		ddo.restrict = 'E';
		ddo.template = '<ul><li ng-repeat="titulo in titulos">{{titulo}}</li></ul>';
		ddo.controller = function($scope, recursoFoto) {
			recursoFoto.query(function(fotos) {
				$scope.titulos = fotos.map(function(foto) {
					return foto.titulo;
				});
			});
		};
		return ddo;
	});